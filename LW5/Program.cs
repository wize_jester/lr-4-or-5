﻿using System;

namespace LW5
{
    class Program
    {
        static void Main(string[] args)
        {
            Random r = new Random();
            Workerfix worker1 = new Workerfix("Igor", "Islamov", new DateTime(r.Next(1950,2018), r.Next(1,12), r.Next(1,28)), 'M', r.Next(10000,50000), r.Next(2,50), r.Next(1,10));
            Workerfix[] workers = Workerfix.SetInfo();

            Console.WriteLine(worker1.GetInfo());
            Person.GetAllPersons(workers);

            
            Workerfix[] workers1 = Workerfix.SetInfo();
            Person.GetAllPersons(workers1);
            Console.WriteLine("Best Pay: \n" + Workerfix.GetBestWorkerPay(workers1).GetInfo());

            
            Person[] persons = new Person[4];
            persons[0] = new Workerfix("Stepan", "Razin", new DateTime(r.Next(1950, 2018), r.Next(1, 12), r.Next(1, 28)), 'M', r.Next(10000, 50000), r.Next(2, 50), r.Next(1, 10));
            persons[1] = new Workerfix("Ahmed", "Fedotov", new DateTime(r.Next(1950, 2018), r.Next(1, 12), r.Next(1, 28)), 'M', r.Next(10000, 50000), r.Next(2, 50), r.Next(1, 10));
            persons[2] = new Workerfix("Inna", "Vaseva", new DateTime(r.Next(1950, 2018), r.Next(1, 12), r.Next(1, 28)), 'W', r.Next(10000, 50000), r.Next(2, 50), r.Next(1, 10));
            persons[3] = new Workerhow("Vasilevs", "Gasovs", new DateTime(r.Next(1950, 2018), r.Next(1, 12), r.Next(1, 28)), 'M', r.Next(100, 500), r.Next(2, 50), r.Next(1, 10), r.Next(20,100));
            Person.GetAllPersons(persons);

            
            Student[] students = Student.SetInfo();
            Student.GetSubjectsInfo(students);
            Console.WriteLine("Results:");
            Student.TransitionToNextGrade(students);
            Student.GetSubjectsInfo(students);

            Console.ReadKey();
        }
    }
}
