﻿using System;

namespace LW5
{
    class Workerfix : Person
    {
        public int Salary { get; set; }

        int bonus;
        public int Bonus 
        {
            get
            {
                return bonus;
            }
            set
            {
                if (value >= 0 && value <= 100)
                    bonus = value;
                else
                    throw new Exception();
            }
        }

        int seniority;
        public int Seniority 
        {
            get
            {
                return seniority;
            }
            set
            {
                if (value >= 0)
                    seniority = value;
                else
                    throw new Exception();
            }
        }
        
        public Workerfix() { }

        public Workerfix(string firstname, string lastname, DateTime dob, char gender, int salary, int bonus, int senioriry)
            : base(firstname, lastname, dob, gender)
        {
            Salary = salary;
            Bonus = bonus;
            Seniority = senioriry;
        }
        
        public override string GetInfo() 
        {
            return base.GetInfo() + "Salary: " + Salary + ", Bonus: " + Bonus + "%, Seniority: " + Seniority + " years" + "\nPay: " + Pay + ", Tax: " + Tax + ", Final Pay: " + FinalPay + '\n';
        }

        public static Workerfix GetBestWorkerPay(Workerfix[] collection) 
        {
            int tmp = 0;
            for (int i = 1; i < collection.Length; i++)
            {
                if (collection[i].FinalPay > collection[tmp].FinalPay)
                    tmp = i;
            }

            return collection[tmp];
        }
       
        public int Pay
        {
            get
            {
                return Salary + (Salary * Bonus) / 100;
            }
        }

        public int Tax
        {
            get
            {
                return (Pay * 13) / 100;
            }
        }

        public virtual int FinalPay
        {
            get
            {
                return Pay - Tax;
            }
        }

        public int BonusUpgrade()
        {
            if (Salary > 10)
                return Bonus * 2;
            return Bonus;
        }

        
        public new static Workerfix[] SetInfo()
        {
            Person[] persons = Person.SetInfo();
            Workerfix[] persons1 = new Workerfix[persons.Length];

            Console.Write("\nWorker:\n\n");
            bool isUpgradeBonus = false;

            Console.Write("Upgrade bonus, if Salary > 10? (Y): ");
            if (char.Parse(Console.ReadLine()) == 'Y')
                isUpgradeBonus = true;

            for (int i = 0; i < persons.Length; i++)
            {
                persons1[i] = new Workerfix(persons[i].FirstName, persons[i].LastName, persons[i].Dob, persons[i].Gender, 0, 0, 0);
                Console.WriteLine("\n{0}: {1} {2}.", i, persons[i].LastName, persons[i].FirstName[0]);

                Console.Write(i + ": Salary: ");
                persons1[i].Salary = int.Parse(Console.ReadLine());

                Console.Write(i + ": Seniority: ");
                persons1[i].Seniority = int.Parse(Console.ReadLine());

                Console.Write(i + ": Bonus: ");
                persons1[i].Bonus = int.Parse(Console.ReadLine());
                if (isUpgradeBonus && persons1[i].Seniority > 10)
                    persons1[i].Bonus *= 2;
            }

            return persons1;
        }
    }
}
