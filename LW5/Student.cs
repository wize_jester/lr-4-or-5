﻿using System;

namespace LW5
{
    class Student : Person
    {
        public int Grade { get; set; }
        public string[] Subjects { get; set; }
        public int[] Marks { get; set; }

        public int SubjectsNumber
        {
            get
            {
                return Subjects.Length;
            }
        }

        public Student() { }
        public Student(string firstname, string lastname, DateTime dob, char gender) : base(firstname, lastname, dob, gender) { }
        public Student(string firstname, string lastname, DateTime dob, char gender, int grade, string[] subjects, int[] marks) : base(firstname, lastname, dob, gender)
        {
            Grade = grade;
            Subjects = subjects;
            Marks = marks;
        }
        public static void GetSubjectsInfo(Student[] students)
        {
            for (int i = 0; i < students.Length; i++)
            {
                Console.WriteLine(students[i].GetInfo());
                Console.WriteLine(i + ": Grade: " + students[i].Grade + "\nSubjects:");
                for (int j = 0; j < students[i].SubjectsNumber; j++)
                {
                    Console.WriteLine(i + ": " + students[i].Subjects[j] + ": " + students[i].Marks[j]); 
                }
            }
        }
        public new static Student[] SetInfo()
        {
            Person[] persons = Person.SetInfo();
            Student[] persons1 = new Student[persons.Length];

            Console.Write("\nStudent:\n");

            for (int i = 0; i < persons.Length; i++)
            {
                persons1[i] = new Student(persons[i].FirstName, persons[i].LastName, persons[i].Dob, persons[i].Gender);
                Console.WriteLine("\n{0}: {1} {2}.", i, persons[i].LastName, persons[i].FirstName[0]);

                Console.Write(i + ": Grade: ");
                persons1[i].Grade = int.Parse(Console.ReadLine());

                Console.Write(i + ": Subjects: ");
                persons1[i].Subjects = Console.ReadLine().Trim().Split(',');

                Console.WriteLine(i + ": Marks:");
                persons1[i].Marks = new int[persons1[i].Subjects.Length];
                for (int j = 0; j < persons1[i].Subjects.Length; j++)
                {
                    Console.Write("\t" + i + ": " + persons1[i].Subjects[j] + ": ");
                    persons1[i].Marks[j] = int.Parse(Console.ReadLine());
                }
            }

            return persons1;
        }

        public static Student[] TransitionToNextGrade(Student[] students)
        {
            for (int i = 0; i < students.Length; i++)
            {
                bool isTransition = true;
                for (int j = 0; j < students[i].Marks.Length; j++)
                {
                    if (students[i].Marks[j] == 2)
                        isTransition = false;
                }
                if (isTransition)
                {
                    if (students[i].Grade < 11)
                    {
                        students[i].Grade++;
                        Array.Clear(students[i].Subjects, 0, students[i].Subjects.Length);
                        Array.Clear(students[i].Marks, 0, students[i].Marks.Length);
                    }
                    else if (students[i].Grade == 11)
                        students[i].Grade++;
                }
                else
                {
                    Array.Clear(students[i].Subjects, 0, students[i].Subjects.Length);
                    Array.Clear(students[i].Marks, 0, students[i].Marks.Length);
                }
            }

            return students;
        }
    }
}
